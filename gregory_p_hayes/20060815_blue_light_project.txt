blue light project
gregory p. hayes
20060815

01. The Who - "Young Man Blues"
02. John Mayall & the Bluesbreakers - "All Your Love"
03. B.B. King - "The Thrill is Gone"
04. Muddy Waters - "Mannish Boy"
05. Led Zeppelin - "The Girl I Love She Got Long Black Wavy Hair"
06. Bob Dylan - "Highway 61 Revisited"
07. John Lee Hooker - "Boom Boom"
08. Buddy Guy - "Red House"
09. Jimi Hendrix - "Hear My Train a Comin'"
10. Eric Clapton - "Blues Before Sunrise"
11. Albert King & John Lee Hooker - "One Bourbon, One Scotch, One Beer"
12. Muddy Waters & the Rolling Stones - "Champagne & Reefer"
13. The Rolling Stones - "Fancy Man Blues"
14. Bobby "Blue" Bland - "Farther Up the Road"
15. Stevie Ray Vaughn - "Mary Had a Little Lamb"
16. Eric Clapton & B.B. King - "Ten Long Years"
17. The Doors - "Back Door Man"
18. Jeff Beck Group - "Ain't Superstitious"
19. Buddy Guy & Junior Wells - "Catfish Blues (Acoustic)"

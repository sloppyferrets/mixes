helter skelter (hymns for the heathen)
earl grey
20060629

01. William Elliott Whitmore - "Cold and Dead"
02. The Amboy Dukes - "Mississippi Murderer"
03. Lynyrd Skynyrd - "Saturday Night Special"
04. Clap Your Hands Say Yeah - "Upon This Tidal Wave of Young Blood"
05. Cursive - "Hymns for the Heathen"
06. Son House - "Death Letter"
07. Nick Cave and the Bad Seeds - "Song of Joy"
08. Okkervil River - "For Real" (Live, KEXP)
09. Tom Waits - "Rain Dogs"
10. The Beatles - "Helter Skelter"
11. Johnny Cash - "Folsom Prison Blues"
12. Danzig - "Thirteen"
13. Drive-By Truckers - "Lookout Mountain"
14. Uncle Tupelo - "No Depression" (Carter Family cover)
15. The White Stripes - "Dead Leaves and the Dirty Ground"
16. Streetlight Manifesto - "A Moment of Violence"
17. Sublime - "April 29, 1992 (Miami)"
18. Shawn Mullins - "Cold Black Heart"
19. The Rolling Stones - "Let it Bleed"
20. Robert Johnson - "Hellhound on My Trail"

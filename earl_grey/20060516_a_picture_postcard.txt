a picture postcard
earl grey
20060516

01. Norman Greenbaum - "Spirit in the Sky"
02. Kings of Leon - "Molly's Chambers"
03. Jimi Hendrix Experience - "Like a Rolling Stone" (Dylan cover)
04. The National - "The Geese of Beverly Road"
05. Old Crow Medicine Show - "Take 'em Away"
06. Cat Power - "The Greatest"
07. Don McLean - "Vincent"
08. The Promise Ring - "A Picture Postcard"
09. Rainer Maria - "Breakfast of Champions"
10. Death Cab for Cutie - "Title and Registration"
11. Desaparecidos - "Man and Wife, the Latter (Damaged Goods)"
12. Hot Water Music - "Radio" (Alkaline Trio cover)
13. Reel Big Fish - "Beer"
14. Dispatch - "The General"
15. The Promise Ring - "Tell Everyone We're Dead"
16. The White Stripes - "Hotel Yorba"
17. Dar Williams - "Highway Patrolman"
18. Bob Dylan and Johnny Cash - "Ring of Fire"
19. Mountain Goats - "Hast Thou Considered the Tetrapod"
20. Uncle Tupelo - "Life Worth Living"
21. Cake - "Stickshifts and Safetybelts"

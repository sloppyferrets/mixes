i see a darkness
earl grey
20060418

01. Cursive - "The Martyr"
02. The Get Up Kids - "I'm A Loner Dottie, A Rebel"
03. Alkaline Trio - "She Took Him to the Lake"
04. Okkervil River - "For the Captain"
05. Dave Matthews - "Gravedigger"
06. Brigth Eyes - "Waste of Paint"
07. David Bowie - "Life on Mars?"
08. Weezer - "Across the Sea"
09. My Morning Jacket - "Golden"
10. Interpol - "Leif Erikson"
11. Braid - "My Baby Smokes"
12. Johnny Cash - "I See A Darkness" (Bonnie Prince Billy cover)
13. Lullaby for the Working Class - "Drama of Your Life"
14. Will Oldham - "In My Mind" (David Allan Coe cover I think)
15. Josh Ritter - "Girl in the War"
16. Muddy Waters - "I Just Want to Make Love to You"
17. Andrew Bird's Bowl of Fire - "11:11"
18. Wilco - "She's a Jar"
19. Matt Pond PA - "In the Aeroplane Over the Sea" (Neutral Milk Hotel cover)
20. Kristin Hersh & Michael Stipe - "Your Ghost"

memories are films about ghosts
earl grey
20060429

01. Counting Crows - "Mrs. Potter's Lullaby"
02. The Promise Ring - "Things Just Getting Good"
03. Straylight Run - "Existentialism on Prom Night"
04. The Postal Service - "The District Sleeps Alone Tonight" (Morning Becomes Ecelectic)
05. The White Stripes - "Seven Nation Army"
06. Jimmy Eat World - "Bleed American"
07. The Clash - "Rock the Casbah"
08. Butthole Surfers - "Pepper"
09. The Ataris - "San Dimas High School Football Rules"
10. Dashboard Confessional - "Carry This Picture"
11. Box Car Racer - "I Feel So"
12. Alkaline Trio - "Jaked on Green Beers"
13. Farside - "I Hope You're Unhappy"
14. The Get Up Kids - "Central Standard Time"
15. Drive-By Truckers - "Zip City"
16. Blink 182 - "I Miss You"
17. Coldplay - "Green Eyes"
18. Oasis - "Wonderwall"
19. Weezer - "Across the Sea"

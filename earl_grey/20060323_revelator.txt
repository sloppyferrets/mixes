revelator
earl grey
20060323

01. Tapes 'n Tapes - "Insistor"
02. Gillian Welch - "Look at Miss Ohio"
03. New Amsterdams - "Idaho"
04. Leadbelly - "Where Did You Sleep Last Night?" (Nirvana cover).... (Kidding)
05. Doobie Brotheres - "Black Water"
06. Bright Eyes - "Bowl of Oranges"
07. The Decemberists - "Eli, the Barrow Boy"
08. The Arcade Fire - "Rebellion (Lies)"
09. Ryan Adams - "Revelator" (Gillian Welch cover)
10. Drive-By Truckers - "Cottonseed"
11. Uncle Tupelo - "Screen Door"
12. Belle & Sebastian - "Whiskey in the Jar" (traditional)
13. Neil Young - "Time Fades Away"
14. Colin Meloy - "Everything I Try to D0, Nothing Seems to Turn Out Right" (Live)
15. Guster - "Come Downstairs and Say Hello"
16. Feist - "Mushaboom"
17. Will Sheff - "For Real" (Live)
18. The Promise Ring - "Become One Anything One Time"

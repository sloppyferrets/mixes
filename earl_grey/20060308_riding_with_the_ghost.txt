riding with the ghost
earl grey
20060308

01. Led Zeppelin - "Babe I'm Gonna Leave You
02. Songs: Ohia - "I've Been Riding With the Ghost"
03. Simon & Garfunkel - "The Only Living Boy in New York"
04. Led Zeppelin - "Tangerine"
05. Belle & Sebastian - "Sukie in the Graveyard"
06. Neutral Milk Hotel - "Ghost"
07. Pink Floyd - "Brain Damage"
08. Nick Cave & the Bad Seeds - "Henry Lee"
09. Jeff Buckley - "Hallelujah"
10. Neil Young - "The Needle and the Damage Done" (live)
11. Okkervil River - "The Velocity of Saul at the Time of his Ascension"
12. Modest Mouse - "Alone Down There"
13. The Rolling Stones - "Paint it Black"
14. Interpol - "Evil"
15. Bright Eyes - "A Perfect Sonnet"
16. Songs: Ohia - "Peoria Lunch Box Blues"
17. Wilco - "Red-Eyed and Blue"
18. Uncle Tupelo - "Still Be Around"
19. Brand New - "The Boy Who Blocked His Own Shot"
20. Tom Petty & the Heartbreakers - "Mary Jane's Last Dance"

sunday mornin' comin' down
earl grey
20060506

01. Bad Religion - "American Jesus"
02. Nick Cave and the Bad Seeds - "In the Ghetto" (Elvis cover)
03. The National - "Karen"
04. Kris Kristofferson - "Sunday Mornin' Comin' Down"
05. Neutral Milk Hotel - "In the Aeroplane Over the Sea" (Live)
06. Tim Hardin - "Lady Came From Baltimore"
07. Okkervil River - "Black"
08. Get Up Kids - "I'll Catch You"
09. Sonic Youth - "Eric's Trip"
10. Old Crow Medicine Show - "Tell it to Me"
11. The Mr. T Experience - "You Today"
12. Alasdair Roberts - "I Walked Abroad in an Evil Hour"
13. Elliott Smith - "Roman Candle"
14. Bonnie 'Prince' Billy - "I See a Darkness"
15. Bruce Springsteen - "Thunder Road" (Acoustic)
16. Otis Redding - "Satisfaction" (Rolling Stones cover)
17. Something Corporate - "Konstantine"
18. Ben Folds Five - "Smoke"
19. Taj Mahal - "Let the Four Winds Blow"

sparks: a rock 'n roll adventure
earl grey
20060605

01. The Who - "Sparks"
02. Jimi Hendrix - "Purple Haze"
03. The Doors - "Break On Through"
04. The Rolling Stones - "Gimme Shelter"
05. Tom Petty & the Heartbreakers - "Refugee"
06. Led Zeppelin - "D'Yer Mak'er" footnote 1 (below)
07. The Beatles - "Yer Blues"
08. The Rolling Stones - "Sympathy for the Devil"
09. Lynyrd Skynyrd - "Gimme Back My Bullets"
10. The Who - "Summertime Blues (Live)"
11. The Doors - "Roadhouse Blues (Live)"
12. Jimi Hendrix - "All Along the Watchtower"
13. Bob Dylan - "Subterranean Homesick Blues"
14. Cream - "Sunshine of Your Love"
15. The Who - "My Generation"
16. The Rolling Stones - "Paint it Black"
17. Led Zeppelin - "Black Dog"
18. Tom Petty & the Heartbreakers - "Mary Jane's Last Dance"
19. Bob Dylan - "Like a Rolling Stone"

peacekeepers
angryPanda
20160320

01. "an honest man" -- fantastic negrito
02. "live love" -- pacific gas & electric
03. "them changes" -- thundercat, flying lotus, & kamasi washington
04. "rhythm bruises" -- har mar superstar
05. "promises" -- monophonics
06. "the sun is shining down" -- jj grey & mofro
07. "you, in weird cities" -- jeff rosenstock
08. "repent walpurgis" -- procol harum
09. "blue cars" -- laymen terms
10. "ma jolie" -- bear vs. shark
11. "the golden throne" -- temples
12. "across the universe" -- the beatles
13. "lousy connection" -- ezra furman
14. "any emotions" -- mini mansions & brian wilson
15. "lowly road" -- phil cook
16. "truckin'" -- grateful dead
17. "the hurricane" -- jj grey & mofro
18. "motor city's burning" -- pacific gas & electric

the invisible cd
angryPanda
20051114

01. "intro: a call to arms" - bandits of the acoustic revolution
02. "here's to life" - streetlight manifesto
03. "IV" - the decemberists
04. "the widow" - mars volta
05. "refused are fucking dead" - refused
06. "ghost of stephen foster" - squirrel nut zippers
07. "damaged goods (emi version)" - gang of four
08. "where the waves are highest" - common rider
09. "pimps" - the coup
10. "them belly full (but we hungry)" - bob marley
11. "black cadillacs" - modest mouse
12. "kissing the lipless" - shins
13. "hey" - the pixies
14. "catamaran" - bear vs shark
15. "falling down" - mad caddies
16. "all along the watchtower" - bob dylan
17. "paddy's lament" - sinead o connor
18. "the hellion-electric eye" - judas priest
19. "creep" - damien rice (radiohead cover)
20. "oh my lover" - pj harvey
21. "les os" - the unicorns
22. "summer of '93" - dead betties

https://www.dropbox.com/s/nz6tx6je1i23qs2/20051114_the_invisible_cd.tar.gz?dl=0

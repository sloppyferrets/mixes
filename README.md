# Mixes #

### angryPanda's mixes ###
* [the invisible cd <nov '05>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20051114_the_invisible_cd.txt)
* [mechanical feelings pressed to plastic <dec ’05>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20051203_mechanical_feelings_pressed_to_plastic.txt)
* [spicy blue salsa \#3 <jan ’06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20060108_spicy_blue_salsa_%23_3.txt)
* [enjoy your left testicle while you still can <jan ’06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20060125_enjoy_your_left_testicle_while_you_still_can.txt)
* [thoughts rest unannounced <mar ’06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20060302_thoughts_rest_unannounced.txt)
* [and she drew(when i’m dead i’ll rest) <apr ’06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20060402_and_she_drew.txt)
* [always taking the listener for a drive <apr ’06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20060424_always_taking_the_listener_for_a_drive.txt)
* [as time was passing, we knew that it would kill us <jul ’06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20060730_as_time_was_passing.txt)
* [to get where you are <may '14>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20140513_to_get_where_you_are.txt)
* [falsely yours, <aug '14>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20140803_falsely_yours.txt)
* [off to sea <jun '15>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20150613_off_to_sea.txt)
* [peacekeepers <mar '16>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/angryPanda/20160320_peacekeepers.txt)

### earl grey's mixes ###
* [stormy weather mix <nov '05>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20051115_stormy_weather_mix.txt)
* [i want your skull <dec '05>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20051213_i_want_your_skull.txt)
* [rid you from my bones <jan '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060116_rid_you_from_my_bones.txt)
* [say you miss me <jan '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060124_say_you_miss_me.txt)
* [wish you were here <feb '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060205_wish_you_were_here.txt)
* [alone again or <feb '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060209_alone_again_or.txt)
* [a quick one while he's away <feb '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060225_a_quick_one_while_hes_away.txt)
* [riding with the ghost <mar '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060308_riding_with_the_ghost.txt)
* [everyone's a building burning (memories of you) <mar '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060314_everyones_a_building_burning.txt)
* [revelator <mar '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060323_revelator.txt)
* [lover i don't have to love <mar '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060330_lover_i_dont_have_to_love.txt)
* [gravity's gone <apr '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060404_gravitys_gone.txt)
* [heaven adores you <apr '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060407_heaven_adores_you.txt)
* [i see a darkness <apr '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060418_i_see_a_darkness.txt)
* [memories are films about ghosts <apr '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060429_memories_are_films_about_ghosts.txt)
* [sunday mornin' comin' down <may '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060506_sunday_mornin_comin_down.txt)
* [a picture postcard <may '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060516_a_picture_postcard.txt)
* [you talk sunshine, i breath fire <may '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060523_you_talk_sunshine_i_breath_fire.txt)
* [as good as dead <may '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060529_as_good_as_dead.txt)
* [sparks: a rock 'n roll adventure <jun '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060605_sparks_a_rocknroll_adventure.txt)
* [grace cathedral hill <jun '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060619_grace_cathedral_hill.txt)
* [helter skelter <jun '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060629_helter_skelter.txt)
* [got on my dead man's suit <jul '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/earl_grey/20060717_got_on_my_dead_mans_suit.txt)

### gregory p. hayes' mixes ###
* [blue light project <aug '06>](https://bitbucket.org/sloppyferrets/mixes/raw/23c239200d1b201873d63b602d9cc692cb1b2243/gregory_p_hayes/20060815_blue_light_project.txt)
